# Simple File Repository service

This project is a Maven based Java project of a Web Service.
This WebLab WebService is a file system repository in charge of saving and granting access to WebLab resource.

Just configure the file system folder and the component will save and load resources from files in this folder.
This implementation is able to save and get resources.
The limitation are that it is not assigning an new URI. It hashes the existing one to save the files. It's also not able to get or save sub resources since it uses the hash as key.

# Build status
[![build status](https://gitlab.ow2.org/weblab/services/simple-file-repository/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/services/simple-file-repository//commits/develop)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
