/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service;

import java.io.File;

import javax.jws.WebService;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.messages.Messages;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceReturn;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceReturn;
import org.ow2.weblab.service.constants.Constants;
import org.purl.dc.elements.DublinCoreAnnotator;


/**
 * This is a simple implementation of a ResourceContainer.
 *
 * It enables to save resource on a file system and get them after.
 * It is a simpler version that the file-repository one.
 * Main differences are:
 * <ul>
 * <li>This version does not allocate uris; it means that incoming URIs has to be unique otherwise their will be some replacements. It maybe be useful for some applications to use the other one if you
 * have multiple resource provider that are not able to generate unique URIs.</li>
 * <li>This version does not enable to get a subresource. It means that if you just want to change an annotation on a document, should shall re-save the whole document. The FileRepository has this
 * functionality.</li>
 * </ul>
 *
 * @author ymombrun, Copyright Cassidian
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.ResourceContainer")
public class SimpleRepo extends AbstractRepository implements ResourceContainer {


	/**
	 * Instance of WebLabMarshaller used to read resources stored in repo.
	 */
	protected final WebLabMarshaller reader;


	/**
	 * Instance of WebLabMarshaller used to store resources in repo.
	 */
	protected final WebLabMarshaller writer;


	/**
	 * The constructor.
	 *
	 * @param path
	 *            The path of the base directory of the repository where resources are stored. Please consider using an absolute path.
	 */
	public SimpleRepo(final String path) {
		this(path, true, true);
	}


	/**
	 * The constructor.
	 *
	 * @param path
	 *            The path of the base directory of the repository where resources are stored. Please consider using an absolute path.
	 * @param validateReader
	 *            Whether or not to validate resources while unmarshaling previously stored resources. Default value is true.
	 * @param validateWriter
	 *            Whether or not to validate resources while marshaling a resource to store. Default value is true.
	 */
	public SimpleRepo(final String path, final boolean validateReader, final boolean validateWriter) {
		this(path, validateReader, validateWriter, 1);
	}



	/**
	 * The constructor.
	 *
	 * @param path
	 *            The path of the base directory of the repository where resources are stored. Please consider using an absolute path.
	 * @param validateReader
	 *            Whether or not to validate resources while unmarshaling previously stored resources. Default value is true.
	 * @param validateWriter
	 *            Whether or not to validate resources while marshaling a resource to store. Default value is true.
	 * @param subFolderDistribution
	 *            The number of character to be used from the SHA1 to create a subfolder distribution inside path folder
	 */
	public SimpleRepo(final String path, final boolean validateReader, final boolean validateWriter, final int subFolderDistribution) {
		super(path, subFolderDistribution);
		this.reader = new WebLabMarshaller(validateReader);
		this.writer = new WebLabMarshaller(validateWriter);
		this.logger.info("'<"+Constants.SERVICE_NAME+">'" + "Started successfully and is ready to use.");
		this.logger.info(Messages.getString(this.resourceBundle, MessagesKeys.REPO_SERVICE_STARTED_1, this.repoBase.getAbsolutePath()));
	}


	@Override
	public LoadResourceReturn loadResource(final LoadResourceArgs args) throws InvalidParameterException, UnexpectedException {

		// Extract the resource to get
		final String uri = this.checkGetResourceArgs(args);

		this.logger.trace("Try to retrieve resource from URI '" + uri + "'.");
		this.logger.debug("'<"+Constants.SERVICE_NAME+">'" + " Start processing document "+"'<"+uri+">'");

		// Get the abstract file path to read
		final File file = this.uriToFile(args.getUsageContext(), uri);

		this.logger.trace("Try to extract resource from file '" + file.getAbsolutePath() + "'.");

		// Extract the resource contained by file
		final Resource res = this.readFile(file, uri);

		this.logger.trace("Return the resource having '" + uri + "' has URI and read from file '" + file.getAbsolutePath() + "'.");
		this.logger.debug("'<"+Constants.SERVICE_NAME+">'" + " End processing document "+"'<"+uri+">'");
		final LoadResourceReturn grr = new LoadResourceReturn();
		grr.setResource(res);
		return grr;
	}


	@Override
	public SaveResourceReturn saveResource(final SaveResourceArgs args) throws InvalidParameterException, UnexpectedException {
		// Extract Resource to save
		final Resource res = this.checkSaveResourceArgs(args);

		final String resourceUri = res.getUri();
		this.logger.trace("Try to save resource with URI '" + resourceUri + "'.");
		final String source = new DublinCoreAnnotator(res).readSource().firstTypedValue();
		this.logger.debug("'<"+Constants.SERVICE_NAME+">'" + " Start processing document "+"'<"+resourceUri+">'" + " - " + "'<"+source+">'");
		// Get the abstract file path to write
		final File file = this.uriToFile(args.getUsageContext(), resourceUri);

		this.logger.trace("File '" + file.getAbsolutePath() + "' will be used.");

		// Write the resource into the file
		this.writeFile(file, res);

		this.logger.trace("Return the URI '" + resourceUri + "' of the resource that has been successfully stored in file '" + file.getAbsolutePath() + "'.");
		this.logger.debug("'<"+Constants.SERVICE_NAME+">'" + " End processing document "+"'<"+resourceUri+">'" + " - " + "'<"+source+">'");
		final SaveResourceReturn srr = new SaveResourceReturn();
		srr.setResourceId(resourceUri);
		return srr;
	}


	/**
	 * Checks that the args is correct. I.e. contains a non null URI.
	 *
	 * @param args
	 *            Wrapper of resourceId
	 * @return The resourceId in args
	 * @throws InvalidParameterException
	 *             If args or resourceId in args is <code>null</code>.
	 */
	protected String checkGetResourceArgs(final LoadResourceArgs args) throws InvalidParameterException {
		if (args == null) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_ARGS_NULL_1, "LoadResourceArgs");
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg);
		}
		final String uri = args.getResourceId();
		if (uri == null) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_ARGS_NULL_1, "LoadResourceArgs.ResourceId");
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg);
		}
		return uri;
	}


	/**
	 * Checks that args contains a resource with a non null uri.
	 *
	 * @param args
	 *            The SaveResourceArgs to test
	 * @return The resource contained by args
	 * @throws InvalidParameterException
	 *             If args, args.getResource() or args.getResource().getUri() is null.
	 */
	protected Resource checkSaveResourceArgs(final SaveResourceArgs args) throws InvalidParameterException {
		if (args == null) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_ARGS_NULL_1, "SaveResourceArgs");
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg);
		}

		if (args.getResource() == null) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_ARGS_NULL_1, "SaveResourceArgs.Resource");
			throw new InvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg);
		}
		final String uri = args.getResource().getUri();
		if (uri == null) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_ARGS_NULL_1, "SaveResourceArgs.Resource.URI");
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg);
		}

		return args.getResource();
	}


	/**
	 * Try to unmarshal a resource from file.
	 *
	 * @param file
	 *            The file to be unmarshaled into a resource
	 * @param uri
	 *            The uri to be used for logs
	 * @return The resource contained in file
	 * @throws UnexpectedException
	 *             If file can't be unmarshaled (in most of the cast due to an IOException).
	 * @throws InvalidParameterException
	 *             If the file does not exists
	 */
	protected synchronized Resource readFile(final File file, final String uri) throws UnexpectedException, InvalidParameterException {
		if (!file.exists()) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.REPO_RES_DONT_EXIST_2, uri, file.getAbsolutePath());
			this.logger.error(msg);
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg);
		}
		try {
			return this.reader.unmarshal(file, Resource.class);
		} catch (final WebLabCheckedException wlce) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.REPO_CANT_READ_RES_2, uri, file.getAbsolutePath());
			throw ExceptionFactory.createUnexpectedException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg, wlce);
		}
	}


	/**
	 * Try to marshal the resource in file.
	 *
	 * @param file
	 *            The file that will contain resource
	 * @param res
	 *            The resource to store on file system
	 * @throws UnexpectedException
	 *             If res was not marshaled into file (in most of the cast due to an IOException).
	 */
	protected synchronized void writeFile(final File file, final Resource res) throws UnexpectedException {
		if (file.exists()) {
			this.logger.debug("The resource '" + res.getUri() + "' already exists. Replacing file '" + file.getAbsolutePath() + "'.");
		}
		try {
			this.writer.marshalResource(res, file);
		} catch (final WebLabCheckedException wlce) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.REPO_CANT_WRITE_RES_2, res.getUri(), file.getAbsoluteFile());
			throw ExceptionFactory.createUnexpectedException("'<"+Constants.SERVICE_NAME+">'" + " Error processing document. "+msg, wlce);
		}
	}

}
