/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service;



/**
 * This class is just a container for the keys of the messages logged in the Simple Repository service.
 *
 * @author ymombrun
 */
public final class MessagesKeys {

	private MessagesKeys() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}


	public static final String ABSTRACT_ARGS_NULL_1 = "AbstractRepository.ArgsNull.1";


	public static final String ABSTRACT_INVALID_DISTRIBUTION_1 = "AbstractRepository.InvalidDistribution.1";


	public static final String ABSTRACT_PATH_NOT_ABSOLUTE_2 = "AbstractRepository.PathNotAbsolute.2";


	public static final String ABSTRACT_PATH_NULL_0 = "AbstractRepository.PathNull.0";


	public static final String ABSTRACT_REPO_CREATION_FAIL_2 = "AbstractRepository.RepoCreationFail.2";


	public static final String ABSTRACT_REPO_NOT_DIR_2 = "AbstractRepository.RepoNotDirectory.2";


	public static final String ABSTRACT_REPO_NOT_RW_2 = "AbstractRepository.RepoNotRW.2";


	public static final String ABSTRACT_SUB_FOLDER_ERR_2 = "AbstractRepository.SubFolderCreationError.2";


	public static final String REPO_CANT_READ_RES_2 = "SimpleRepo.CanNotReadResource.2";


	public static final String REPO_CANT_WRITE_RES_2 = "SimpleRepo.CanNotWriteResource.2";


	public static final String REPO_RES_DONT_EXIST_2 = "SimpleRepo.ResourceDoesNotExists.2";


	public static final String REPO_SERVICE_STARTED_1 = "SimpleRepo.ServiceStarted.1";


	public static final String CLEANER_SERVICE_STARTED_1 = "SimpleRepoCleaner.ServiceStarted.1";


	public static final String CLEANER_DELETE_FAIL_2 = "SimpleRepoCleaner.DeleteFail.2";


	public static final String CLEANER_BAD_URI_0 = "SimpleRepoCleaner.BadUri.0";


	public static final String CLEANER_EMPTY_TRASH_0 = "SimpleRepoCleaner.EmptyTrash.0";

}
