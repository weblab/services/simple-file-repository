/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.jws.WebService;

import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.messages.Messages;
import org.ow2.weblab.core.services.Cleanable;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.cleanable.CleanArgs;
import org.ow2.weblab.core.services.cleanable.CleanReturn;
import org.ow2.weblab.service.constants.Constants;

/**
 * Extension of the repository used for resource removal
 * 
 * @author ymombrun
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Cleanable")
public class SimpleRepoCleaner extends AbstractRepository implements Cleanable {


	/**
	 * The constructor.
	 *
	 * @param path
	 *            The path of the base directory of the repository where resources are stored. Please consider using an absolute path.
	 */
	public SimpleRepoCleaner(final String path) {

		this(path, 1);
	}


	/**
	 * The constructor.
	 *
	 * @param path
	 *            The path of the base directory of the repository where resources are stored. Please consider using an absolute path.
	 * @param subFolderDistribution
	 *            The number of character to be used from the SHA1 has subfolder name
	 */
	public SimpleRepoCleaner(final String path, final int subFolderDistribution) {

		super(path, subFolderDistribution);
		this.logger.info("'<" + Constants.SERVICE_NAME + ">'" + Messages.getString(this.resourceBundle, MessagesKeys.CLEANER_SERVICE_STARTED_1, this.repoBase.getAbsolutePath()));
	}


	@Override
	public CleanReturn clean(final CleanArgs args) throws InvalidParameterException {

		final List<String> trash = this.checkArgs(args);

		for (final String uri : trash) {
			final File file = super.uriToFile(args.getUsageContext(), uri);
			if (!file.exists()) {
				this.logger.trace("File " + file.getPath() + " for uri " + uri + " does not exists. Nothing to do.");
			} else if (file.delete()) {
				this.logger.trace("File " + file.getPath() + " for uri " + uri + " succesfully removed.");
			} else {
				if (file.exists()) {
					this.logger.warn("'<" + Constants.SERVICE_NAME + ">'" + " Unable to properly process document. "
							+ Messages.getString(this.resourceBundle, MessagesKeys.CLEANER_DELETE_FAIL_2, uri, file.getAbsolutePath()));
					file.deleteOnExit();
				} else {
					// File has been removed in the mean time, nothing to do
				}
			}
		}
		return new CleanReturn();
	}


	/**
	 * @param args
	 *            The CleanArgs to test
	 * @return The trash list
	 * @throws InvalidParameterException
	 *             If args is not valid
	 */
	protected List<String> checkArgs(final CleanArgs args) throws InvalidParameterException {

		if (args == null) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_ARGS_NULL_1, "CleanArgs");
			this.logger.error(msg);
			throw ExceptionFactory.createInvalidParameterException("'<" + Constants.SERVICE_NAME + ">'" + " Error processing document. " + msg);
		}

		if (!args.isSetTrash()) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_ARGS_NULL_1, "CleanArgs.Trash");
			this.logger.error(msg);
			throw ExceptionFactory.createInvalidParameterException("'<" + Constants.SERVICE_NAME + ">'" + " Error processing document. " + msg);
		}
		final List<String> trash = new ArrayList<>(args.getTrash()); // Copy the trash list
		final ListIterator<String> trashIterator = trash.listIterator();

		while (trashIterator.hasNext()) {
			final String uri = trashIterator.next();
			if ((uri == null) || uri.isEmpty()) {
				this.logger.warn("'<" + Constants.SERVICE_NAME + ">'" + " Unable to properly process document. " + Messages.getString(this.resourceBundle, MessagesKeys.CLEANER_BAD_URI_0));
				trashIterator.remove();
			}
		}

		if (trash.isEmpty()) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.CLEANER_EMPTY_TRASH_0);
			this.logger.error(msg);
			throw ExceptionFactory.createInvalidParameterException("'<" + Constants.SERVICE_NAME + ">'" + " Error processing document. " + msg);
		}
		return trash;
	}

}
