/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service;


import java.io.File;
import java.util.ResourceBundle;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.messages.Messages;

/**
 * This class is just an abstraction on top of the simple repository and the full data management class.
 *
 * @author ymombrun
 */
public class AbstractRepository {


	private static final String XML = ".xml";


	/**
	 * The resourceBundle that contains the error messages of this service
	 */
	protected final ResourceBundle resourceBundle;


	/**
	 * The logger used inside.
	 */
	protected final Log logger;


	/**
	 * The base folder of the repository in the file system
	 */
	protected final File repoBase;


	private final int subFolderDistribution;



	/**
	 * The constructor.
	 *
	 * @param path
	 *            The path of the base directory of the repository where resources are stored. Please consider using an absolute path.
	 * @param subFolderDistribution
	 *            The number of character to be used from the SHA1 has subfolder name
	 */
	public AbstractRepository(final String path, final int subFolderDistribution) {
		this.logger = LogFactory.getLog(this.getClass());
		this.resourceBundle = ResourceBundle.getBundle(AbstractRepository.class.getCanonicalName());
		if (path == null) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_PATH_NULL_0);
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}

		if ((subFolderDistribution < 1) || (subFolderDistribution > 16)) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_INVALID_DISTRIBUTION_1, String.valueOf(subFolderDistribution));
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}

		final File tempRepoPath = new File(path);
		this.repoBase = tempRepoPath.getAbsoluteFile();
		if (!tempRepoPath.isAbsolute()) {
			this.logger.warn(Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_PATH_NOT_ABSOLUTE_2, this.repoBase, tempRepoPath));
		}

		if (!this.repoBase.exists() && !this.repoBase.mkdirs()) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_REPO_CREATION_FAIL_2, this.repoBase, path);
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}
		if (!this.repoBase.isDirectory()) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_REPO_NOT_DIR_2, this.repoBase, path);
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}
		if (!(this.repoBase.canRead() && this.repoBase.canWrite())) {
			final String msg = Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_REPO_NOT_RW_2, this.repoBase, path);
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}
		this.subFolderDistribution = subFolderDistribution;
	}


	/**
	 * @param usageContext
	 *            Not used for the moment
	 * @param uri
	 *            The uri to convert into a path
	 * @return A file descriptor, whether it exists or not
	 */
	protected File uriToFile(final String usageContext, final String uri) {
		// Creates a String of 64 chars
		final String sha = DigestUtils.sha256Hex(uri);

		final File folder = new File(this.repoBase, sha.substring(0, this.subFolderDistribution));
		if (!folder.exists() && !folder.mkdirs()) {
			this.logger.warn(Messages.getString(this.resourceBundle, MessagesKeys.ABSTRACT_SUB_FOLDER_ERR_2, folder.getAbsolutePath(), uri));
		}

		// The file in folder.
		return new File(folder, sha + AbstractRepository.XML);
	}

}