/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.messages.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.messages.Messages;
import org.ow2.weblab.service.AbstractRepository;
import org.ow2.weblab.service.MessagesKeys;


/**
 * Test that every messages exists.
 *
 * @author WebLab Cassidian Team
 * @date 2012-02-09
 */
public class MessagesTest {


	protected final Log logger = LogFactory.getLog(MessagesTest.class);


	@Test
	public void checkEveryKeys() throws IllegalArgumentException, IllegalAccessException {
		final ResourceBundle wlmBundle = ResourceBundle.getBundle(AbstractRepository.class.getCanonicalName());
		for (final Field field : MessagesKeys.class.getDeclaredFields()) {
			if (!field.getType().equals(String.class)) {
				this.logger.debug("Skipping field: " + field.getName());
				continue;
			}
			final String value = (String) field.get(null); // null since fields are static
			final String message = Messages.getString(wlmBundle, value);
			Assert.assertFalse("Key " + value + " not found.", message.startsWith("!") && message.endsWith("!"));
		}
	}


	@Test
	public void testNotExistingKey() {
		final ResourceBundle bundle = ResourceBundle.getBundle(AbstractRepository.class.getCanonicalName());
		final String key = "NotFound";
		final String excl = "!";
		Assert.assertEquals("The message should state that the key has not been found.", excl + key + excl, Messages.getString(bundle, key));
		Assert.assertEquals("The message should state that the key has not been found.", excl + key + excl + Arrays.toString(new String[] { key, key }), Messages.getString(bundle, key, key, key));
	}


	@Test
	public void testConstructor() throws Exception {
		try {
			for (final Constructor<?> c : MessagesKeys.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail("Cause of failure was not an UnsupportedOperationException but a " + ite.getCause());
			}
		}
	}

}
