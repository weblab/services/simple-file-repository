/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.test;

import org.junit.Test;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.service.SimpleRepo;

/**
 * LimitsTest.java
 *
 * @author ymombrun
 * @date 2010-04-15
 */
public class LimitsTest {


	final ResourceContainer repo = new SimpleRepo("target/testRepo");


	@Test(expected = InvalidParameterException.class)
	public void testLoadNull() throws Exception {
		this.repo.loadResource(null);
	}


	@Test(expected = InvalidParameterException.class)
	public void testLoadResourceArgsEmpty() throws Exception {
		this.repo.loadResource(new LoadResourceArgs());
	}


	@Test(expected = InvalidParameterException.class)
	public void testLoadNonExistingResource() throws Exception {
		LoadResourceArgs lra = new LoadResourceArgs();
		lra.setResourceId("Non existing file." + System.nanoTime());
		this.repo.loadResource(lra);
	}


	@Test(expected = InvalidParameterException.class)
	public void testSaveNull() throws Exception {
		this.repo.saveResource(null);
	}


	@Test(expected = InvalidParameterException.class)
	public void testSaveResourceArgsEmpty() throws Exception {
		this.repo.saveResource(new SaveResourceArgs());
	}


	@Test(expected = InvalidParameterException.class)
	public void testSaveResourceWithoutUri() throws Exception {
		final SaveResourceArgs sra = new SaveResourceArgs();
		final Resource res = new Resource();
		sra.setResource(res);
		this.repo.saveResource(sra);
	}

}
