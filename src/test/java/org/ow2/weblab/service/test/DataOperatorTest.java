/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Cleanable;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.cleanable.CleanArgs;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceReturn;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceReturn;
import org.ow2.weblab.service.SimpleRepo;
import org.ow2.weblab.service.SimpleRepoCleaner;

public class DataOperatorTest {


	final Cleanable repoCleaner = new SimpleRepoCleaner("target/testRepoData");


	final ResourceContainer repo = new SimpleRepo("target/testRepoData");


	@Test
	public void testSequence() throws Exception {

		/*
		 * Save a Document
		 */
		final Resource res1 = WebLabResourceFactory.createResource("toto", "tutu", Document.class);
		final String uri1 = res1.getUri();

		final SaveResourceArgs sra = new SaveResourceArgs();
		sra.setResource(res1);

		final SaveResourceReturn srr = this.repo.saveResource(sra);
		Assert.assertEquals(uri1, srr.getResourceId());

		/*
		 * Retrieve the Document
		 */
		final LoadResourceArgs gra = new LoadResourceArgs();
		gra.setResourceId(uri1);

		final LoadResourceReturn grr = this.repo.loadResource(gra);
		Assert.assertEquals(uri1, grr.getResource().getUri());
		Assert.assertEquals(Document.class, grr.getResource().getClass());

		/*
		 * delete the Document
		 */
		final CleanArgs da = new CleanArgs();
		da.getTrash().add(uri1);
		this.repoCleaner.clean(da);

		/*
		 * Try to retrieve the Document
		 */
		try {
			this.repo.loadResource(gra);
			Assert.fail("The Document " + uri1 + " has not been deleted.");
		} catch (final InvalidParameterException ipe) {
			/*
			 * Not doing this test with "expected = InvalidParameterException.class" because if this exception is thrown earlier it should let the test fail.
			 */
			Assert.assertNotNull(ipe);
			Assert.assertNotNull(ipe.getFaultInfo());
			Assert.assertNotNull(ipe.getMessage());
		}

		// Remove it twice, no error should be thrown
		this.repoCleaner.clean(da);
	}


	@Test(expected = InvalidParameterException.class)
	public void testNullClean() throws Exception {
		this.repoCleaner.clean(null);
	}


	@Test(expected = InvalidParameterException.class)
	public void testNoTrash() throws Exception {
		this.repoCleaner.clean(new CleanArgs());
	}


	@Test(expected = InvalidParameterException.class)
	public void testInvalidTrash() throws Exception {
		final CleanArgs ca = new CleanArgs();
		ca.getTrash().add("");
		ca.getTrash().add(null);
		this.repoCleaner.clean(ca);
	}

}
