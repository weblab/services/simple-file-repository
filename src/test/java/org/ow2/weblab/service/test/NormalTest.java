/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.test;


import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceReturn;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceReturn;
import org.ow2.weblab.service.SimpleRepo;

/**
 * NormalTest.java
 *
 * @author ymombrun
 * @date 2010-04-15
 */
public class NormalTest {


	final ResourceContainer repo = new SimpleRepo("target/testRepo");


	@Test
	public void testSequence() throws Exception {

		/*
		 * Save a Document
		 */
		final Resource res1 = WebLabResourceFactory.createResource("toto", "tutu", Document.class);
		final String uri1 = res1.getUri();

		final SaveResourceArgs sra = new SaveResourceArgs();
		sra.setResource(res1);

		SaveResourceReturn srr = this.repo.saveResource(sra);
		Assert.assertEquals(uri1, srr.getResourceId());

		/*
		 * Retrieve the Document
		 */
		final LoadResourceArgs gra = new LoadResourceArgs();
		gra.setResourceId(uri1);

		LoadResourceReturn grr = this.repo.loadResource(gra);
		Assert.assertEquals(uri1, grr.getResource().getUri());
		Assert.assertEquals(Document.class, grr.getResource().getClass());

		/*
		 * Save a PoK with the same URI (override)
		 */
		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("toto", "tutu", PieceOfKnowledge.class);
		Assert.assertEquals(uri1, pok.getUri());

		sra.setResource(pok);

		PoKUtil.setPoKData(pok, "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" />");
		srr = this.repo.saveResource(sra);
		Assert.assertEquals(uri1, srr.getResourceId());

		/*
		 * Retrieve the PoK
		 */
		grr = this.repo.loadResource(gra);
		Assert.assertEquals(uri1, grr.getResource().getUri());
		Assert.assertEquals(PieceOfKnowledge.class, grr.getResource().getClass());
	}


	@Test(expected = InvalidParameterException.class)
	public void testRequestNonExistingResource() throws UnexpectedException, AccessDeniedException, UnsupportedRequestException, ServiceNotConfiguredException,
			InvalidParameterException, InsufficientResourcesException, ContentNotAvailableException {
		final LoadResourceArgs gra = new LoadResourceArgs();
		gra.setResourceId("http://qshtf/qsdkugqsd");
		this.repo.loadResource(gra);
	}

}
