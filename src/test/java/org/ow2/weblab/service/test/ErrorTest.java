/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.test;

import java.io.File;

import org.junit.Test;
import org.ow2.weblab.service.SimpleRepo;
import org.ow2.weblab.service.SimpleRepoCleaner;



/**
 * @author ymombrun
 * @date 2016-03-09
 */
public class ErrorTest {


	@Test(expected = IllegalArgumentException.class)
	public void testNullRepoFolder() throws Exception {
		new SimpleRepo(null);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testBadRepoFolder() throws Exception {
		new SimpleRepo(new File("pom.xml").getAbsolutePath());
	}


	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDistributionFolders() throws Exception {
		new SimpleRepoCleaner("target/testInvalidDistributionFolder", 0);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDistributionFolders2() throws Exception {
		new SimpleRepoCleaner("target/testInvalidDistributionFolder", 17);
	}

}
