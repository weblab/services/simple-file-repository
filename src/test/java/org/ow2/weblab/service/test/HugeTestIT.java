/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.LowLevelDescriptor;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.resourcecontainer.SaveResourceArgs;
import org.ow2.weblab.service.SimpleRepo;
import org.ow2.weblab.service.test.thread.ThreadGet;
import org.ow2.weblab.service.test.thread.ThreadSave;


/**
 * HugeTestIT.java
 *
 * This integration test is
 *
 * @author ymombrun
 * @date 2010-04-15
 */
public class HugeTestIT {


	private static final String COMPLEX_SENTENCE = "Char: fr: è, zh: 官, ar: عر  €.\n\n";


	private static final String SIMPLE_SENTENCE = "Test i times each thread.\n";


	private final static String DEFAULT_PATH = "target/testRepo";


	private final static int DEFAULT_DISTRIBUTION = 3;


	@Test
	public void testVariousResources() throws Exception {
		final ResourceContainer repo = new SimpleRepo(HugeTestIT.DEFAULT_PATH, true, true, DEFAULT_DISTRIBUTION);

		final int nbThread = 50;
		final int nbProcess = 500;
		final List<Callable<String>> writeThreads = new ArrayList<>(nbProcess);

		final StringBuilder sb = new StringBuilder();
		for (int i = 1; i <= nbProcess; i++) {
			sb.append(SIMPLE_SENTENCE);
			sb.append(COMPLEX_SENTENCE);

			final Document doc = WebLabResourceFactory.createResource("SimpleRepoTest", "Thread-" + i, Document.class);

			// Store every documents a first time since get may be call before the save.
			final SaveResourceArgs sra = new SaveResourceArgs();
			sra.setResource(doc);
			repo.saveResource(sra);

			WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class).setContent(sb.toString());

			writeThreads.add(new ThreadSave(i, repo, doc));
		}

		final List<Callable<Resource>> readThreads = new ArrayList<>(nbProcess);
		for (int i = 1; i <= nbProcess; i++) {
			final String uri = "weblab://SimpleRepoTest/Thread-" + i;
			readThreads.add(new ThreadGet(i, repo, uri));
		}


		final ExecutorService wExecutor = Executors.newFixedThreadPool(nbThread);
		final List<Future<String>> wResults = wExecutor.invokeAll(writeThreads, 5L, TimeUnit.MINUTES);

		final ExecutorService rExecutor = Executors.newFixedThreadPool(nbThread);
		final List<Future<Resource>> rResults = rExecutor.invokeAll(readThreads, 5L, TimeUnit.MINUTES);
		for (final Future<String> future : wResults) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			Assert.assertNotNull(future.get());
		}
		for (final Future<Resource> future : rResults) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			final Resource res = future.get();
			Assert.assertNotNull(res);
			Assert.assertTrue(res instanceof Document);
			final Document doc = (Document) res;
			Assert.assertTrue(doc.isSetMediaUnit());
			Assert.assertTrue(!doc.isSetAnnotation());
			Assert.assertTrue(!doc.isSetDescriptor());
			Assert.assertTrue(!doc.isSetSegment());
			Assert.assertEquals(1, doc.getMediaUnit().size());
			final MediaUnit mu = doc.getMediaUnit().iterator().next();
			Assert.assertTrue(!mu.isSetAnnotation());
			Assert.assertTrue(!mu.isSetDescriptor());
			Assert.assertTrue(!mu.isSetSegment());
			Assert.assertTrue(mu instanceof Text);
			final Text t = (Text) mu;
			Assert.assertNotNull(t.getContent());
			Assert.assertTrue(t.getContent().startsWith(SIMPLE_SENTENCE + COMPLEX_SENTENCE));
		}

		wExecutor.shutdownNow();
		rExecutor.shutdown();
	}



	@Test
	public void testThreeResources() throws Exception {
		final ResourceContainer repo = new SimpleRepo(HugeTestIT.DEFAULT_PATH);

		final int nbThread = 100;

		final Document res1 = WebLabResourceFactory.createResource("Test", "res1", Document.class);
		// res3 --> testRepo/e/e1ac2a294c44521fe783ed2e4f7265b85ff2ebddcb31b087224abca73d54a924.xml
		final Text mu = WebLabResourceFactory.createAndLinkMediaUnit(res1, Text.class);
		mu.setContent("Text content: Caractères chi : 官, ara عر  €");
		final LowLevelDescriptor res2 = WebLabResourceFactory.createResource("Test", "res2", LowLevelDescriptor.class);
		// res3 --> testRepo/5/55dcc86f2cea5f2bd9372428bb78572d4ec5da73a787b95ecbb140a17ec41286.xml
		res2.setData("TheKey");

		res2.setData("The Label;Value 1");


		final LinearSegment res3 = SegmentFactory.createAndLinkSegment(mu, LinearSegment.class);
		// res3 --> testRepo/5/5222316b419fe56b674b0952798a6120660d14651eddcb1f80fcbb35a7aa923e.xml
		res3.setEnd(5);
		res3.setStart(1);

		// Save each resource one time first
		final SaveResourceArgs sra = new SaveResourceArgs();
		sra.setResource(res1);
		repo.saveResource(sra);
		sra.setResource(res2);
		repo.saveResource(sra);

		repo.saveResource(sra);

		final List<Callable<String>> writeThreads = new ArrayList<>(nbThread);
		final List<Callable<Resource>> readThreads = new ArrayList<>(nbThread);

		for (int i = 1; i <= nbThread; i++) {
			Resource res = null;
			final double r1 = Math.random();
			if (r1 < 0.33) {
				res = res1;
			} else {
				res = res2;
			}
			if (Math.random() < 0.5) {
				writeThreads.add(new ThreadSave(i, repo, res));
			} else {
				readThreads.add(new ThreadGet(i, repo, res.getUri()));
			}
		}

		final ExecutorService wExecutor = Executors.newFixedThreadPool(nbThread);
		final List<Future<String>> wResults = wExecutor.invokeAll(writeThreads, 5L, TimeUnit.MINUTES);

		final ExecutorService rExecutor = Executors.newFixedThreadPool(nbThread);
		final List<Future<Resource>> rResults = rExecutor.invokeAll(readThreads, 5L, TimeUnit.MINUTES);
		for (final Future<String> future : wResults) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			Assert.assertNotNull(future.get());
		}
		for (final Future<Resource> future : rResults) {
			Assert.assertTrue(future.isDone());
			Assert.assertFalse(future.isCancelled());
			Assert.assertNotNull(future.get());
		}
		wExecutor.shutdownNow();
		rExecutor.shutdown();
	}


}
