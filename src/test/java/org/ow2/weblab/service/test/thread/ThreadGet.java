/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.test.thread;

import java.util.concurrent.Callable;

import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;


/**
 * A callable that internally calls a repo to get a resource.
 *
 * @author ymombrun
 * @date 2010-04-15
 */
public class ThreadGet implements Callable<Resource> {


	private final int id;


	private final ResourceContainer repo;


	final private LoadResourceArgs gra;


	public ThreadGet(final int id, final ResourceContainer repo, final String uri) {
		this.id = id;
		this.repo = repo;
		this.gra = new LoadResourceArgs();
		this.gra.setResourceId(uri);
	}


	@Override
	public Resource call() throws Exception {
		LogFactory.getLog(this.getClass()).info("Thread nb " + this.id);
		return this.repo.loadResource(this.gra).getResource();
	}


}
